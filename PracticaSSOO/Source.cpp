#include <iostream>
#include <Windows.h>
#include <thread>
#include <mutex>
#include <vector>
#include <time.h>
#include <winsock.h>
#include <iostream>
#include <string>
#pragma comment(lib, "Ws2_32.lib")

// kBufferSize must be larger than the length of kpcEchoMessage.
const int kBufferSize = 1024;
const int kShutdownDelay = 3;
SOCKET sd;

using namespace std;
//ASCII for player directions
#define LEFT 27
#define RIGHT 26
#define UP 24
#define DOWN 25

enum class GameState {none, menu, game, rankings, achievements, exit, dead};
enum class Achievements {
	A1, //Matar 2 zombies amb un mateix molotov
	A2, //Matar 3 zombies amb un mateix molotov
	A3, //Matar 4 zombies amb un mateix molotov
	A4, //Sobreviure mig minut en una partida
	A5, //Sobreviure 1 minut en una partida
	A6, //Haver passat per les 4 cantonades en una partida
	A7// No matar ni un zombie en una partida
};

GameState gameState = GameState::menu;
vector<Achievements> vectorAchiev;

struct Player {
	string name;
	int x, y, direction, life, id;
	Player() {
		x = 5;
		y = 5;
		direction = UP;
		life = 100;
	}
};

struct Zombies {
	int x, y, id;
	Zombies() {
		x = 3;
		y = 8;
	}
};

struct Vector2 {
	int x;
	int y;
};

std::mutex mtx;

const char SYMBOL_EMPTY = ' ';
const char SYMBOL_ZOMBIE = 2;
const char SYMBOL_WALL = '#';
const char SYMBOL_FIRE = 'W';
const int MapDx = 20;
const int MapDy = 20;
const int GameSpeed = 300;
const int MaxZombies = 4;

bool molotovThrowed = false;
int zombieskilled = 0;
bool anyZombieKilled = true;
int turn = 0;

Player player; //The player
Zombies zombies[MaxZombies]; //Array of zombies

char map[MapDx][MapDy];

void callServer() {
	// Start Winsock up
	WSAData wsaData;
	WSAStartup(MAKEWORD(1, 1), &wsaData);

	// Find the server's address
	cout << "Looking up address..." << flush;
	u_long nRemoteAddress = inet_addr("127.0.0.1");

	in_addr Address;
	memcpy(&Address, &nRemoteAddress, sizeof(u_long));
	cout << inet_ntoa(Address) << ":" << 15600 << endl;

	// Connect to the server
	cout << "Connecting to remote host..." << flush;
	// Create a stream socket
	sd = socket(AF_INET, SOCK_STREAM, 0);
	sockaddr_in sinRemote;
	sinRemote.sin_family = AF_INET;
	sinRemote.sin_addr.s_addr = nRemoteAddress;
	sinRemote.sin_port = htons(15600);
	connect(sd, (sockaddr*)&sinRemote, sizeof(sockaddr_in));

	cout << "connected, socket " << sd << "." << endl;
}

void sendData(char* string) {
	// Send the echo packet to the server
	cout << "Sending echo packet (" << strlen(string) << " bytes)..." << flush;
	int nBytes;
	send(sd, string, strlen(string), 0);
	cout << endl;
}

void closeClient() {
	// Shut connection down
	cout << "Shutting connection down..." << flush;
	shutdown(sd, 1);
	closesocket(sd);

	cout << "All done!" << endl;

	WSACleanup();
}

void initializeMap() {
	for (int i = 0; i < MapDx; i++) {
		for (int j = 0; j < MapDy; j++) {
			if (i == 0 || j == 0 || i == MapDx - 1 || j == MapDy - 1) {
				map[i][j] = SYMBOL_WALL;
			}
			else {
				map[i][j] = SYMBOL_EMPTY;
			}
		}
	}

	cout << "Introduce your name :" << endl;
	cin >> player.name;

	cout << player.name;

	srand(time(NULL));
	//First respawn for player
	player.x = rand() % 17 + 2;
	player.y = rand() % 17 + 2;
	//First respawn for zombies
	for (int i = 0; i < MaxZombies; i++) {
		zombies[i].x = rand() % 17 + 2;
		zombies[i].y = rand() % 17 + 2;
	}
}
//Coctel function
void ThrowCoctel() {
	while(player.life > 0) {

		if (GetAsyncKeyState(VK_RETURN)) {
			int range = 3;  //Range of coctel explosion
			int coctelX;	//Top x of explosion
			int coctelY;	//Top y of explosion

			molotovThrowed = true;

			for (int i = 1; i <= range; i++) {	//Set coctel explosion in map 

				switch (player.direction) {
					case UP:
						map[player.x][player.y - i] = SYMBOL_FIRE;		//Left x of explosion
						map[player.x - 1][player.y - i] = SYMBOL_FIRE;	//Middle x of explosion
						map[player.x + 1][player.y - i] = SYMBOL_FIRE;	//Right x of explosion
						coctelX = player.x - 1;							//Save top position
						coctelY = player.y - 3;							//Save top position
						break;


					case DOWN:											//The same for DOWN case
						map[player.x][player.y + i] = SYMBOL_FIRE;
						map[player.x - 1][player.y + i] = SYMBOL_FIRE;
						map[player.x + 1][player.y + i] = SYMBOL_FIRE;
						coctelX = player.x - 1;
						coctelY = player.y + 1;
						break;

					case RIGHT:
						map[player.x + i][player.y - 1] = SYMBOL_FIRE;
						map[player.x + i][player.y] = SYMBOL_FIRE;
						map[player.x + i][player.y + 1] = SYMBOL_FIRE;
						coctelX = player.x + 1;
						coctelY = player.y - 1;
						break;

					case LEFT:
						map[player.x - i][player.y - 1] = SYMBOL_FIRE;
						map[player.x - i][player.y] = SYMBOL_FIRE;
						map[player.x - i][player.y + 1] = SYMBOL_FIRE;
						coctelX = player.x - 3;
						coctelY = player.y - 1;
						break;
				}
			}

			std::this_thread::sleep_for(std::chrono::seconds(1));		//Wait for 1 second
			molotovThrowed = false;
			for (int j = 0; j < range; j++) {							//Turn explosion area to SYMBOL_EMPTY
				for (int i = 0; i < range; i++) {
					map[coctelX + i][coctelY + j] = SYMBOL_EMPTY;
				}
			}
		}
	}	
}
//PLayer actions function
void playerMove() {
	bool upRight = false, upLeft = false, downRight = false, downLeft = false;
	int counterFrame = 0;
	while (player.life > 0) {
		//Movement
		if (GetAsyncKeyState(VK_UP) && player.y > 1) {
			if (map[player.x][player.y] != SYMBOL_FIRE)map[player.x][player.y] = SYMBOL_EMPTY;
			player.y--;
			player.direction = UP; //Change the player direction

		}
		if (GetAsyncKeyState(VK_DOWN) && player.y < MapDy - 2) {
			if (map[player.x][player.y] != SYMBOL_FIRE)map[player.x][player.y] = SYMBOL_EMPTY;
			player.y++;
			player.direction = DOWN;
		}
		if (GetAsyncKeyState(VK_RIGHT) && player.x < MapDx - 2) {
			if(map[player.x][player.y] != SYMBOL_FIRE)map[player.x][player.y] = SYMBOL_EMPTY;
			player.x++;
			player.direction = RIGHT;
		}
		if (GetAsyncKeyState(VK_LEFT) && player.x > 1) {
			if (map[player.x][player.y] != SYMBOL_FIRE)map[player.x][player.y] = SYMBOL_EMPTY;
			player.x--;
			player.direction = LEFT;
		}

		if (map[player.x][player.y] == SYMBOL_FIRE)player.life -= 10;		//If player touch fire, -10 life points
		if (map[player.x][player.y] != SYMBOL_FIRE) map[player.x][player.y] = player.direction; //Set the player in the map

		// Achievements 4 corner
		if (player.x == 1 && player.y == 1) {
			upLeft = true;
		} else if (player.x == 18 && player.y == 1) {
			upRight = true;
		} else if (player.x == 18 && player.y == 18) {
			downRight = true;
		} else if (player.x == 1 && player.y == 18) {
			downLeft = true;
		}

		if (upRight && upLeft && downRight && downLeft) {
			vectorAchiev.push_back(Achievements::A6);
			upRight = false;
			upLeft = false;
			downRight = false;
			downLeft = false;
			counterFrame++;
		}
		if (counterFrame >=1 && counterFrame <= 10) {
			counterFrame++;
			cout << "Achievement unlocked!! cross 4 corner" << endl;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));  //GameSpeed
	}
}

//Zombies function
void zombieMove() {
	bool achievementUnlocked = false;
	int counterFrame = 0;
	while (player.life > 0) {

		for (int i = 0; i < MaxZombies; i++) {
			//X movement
			if (zombies[i].x != player.x) {													//If zombie isn't in the same row
				if (zombies[i].x - player.x > 0 && map[zombies[i].x - 1][zombies[i].y] != SYMBOL_ZOMBIE) {											//If the zombie is in the right of player
					if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
					zombies[i].x--;
				}
				if (zombies[i].x - player.x < 0 && map[zombies[i].x + 1][zombies[i].y] != SYMBOL_ZOMBIE) {											//If the zombie is in the left of player
					if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
					zombies[i].x++;
				}
			}
			if (zombies[i].y == player.y && player.x - zombies[i].x > 0 && map[zombies[i].x + 1][zombies[i].y] != SYMBOL_ZOMBIE) {					//If zombie is in the same column
				if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
				zombies[i].x++;
			}
			if (zombies[i].y == player.y && player.x - zombies[i].x < 0 && map[zombies[i].x - 1][zombies[i].y] != SYMBOL_ZOMBIE) {
				if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
				zombies[i].x--;
			}

			//Y movement
			if (zombies[i].y != player.y) {													//If zombie isn't in the same column
				if (zombies[i].y - player.y > 0 && map[zombies[i].x][zombies[i].y-1] != SYMBOL_ZOMBIE) {
					if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
					zombies[i].y--;
				}
				if (zombies[i].y - player.y < 0 && map[zombies[i].x][zombies[i].y + 1] != SYMBOL_ZOMBIE) {
					if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
					zombies[i].y++;
				}
			}

			if (zombies[i].x == player.x && player.y - zombies[i].y > 0 && map[zombies[i].x][zombies[i].y + 1] != SYMBOL_ZOMBIE) {
				if (map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
				zombies[i].y++;
			}
			if (zombies[i].x == player.x && player.y - zombies[i].y < 0 && map[zombies[i].x][zombies[i].y - 1] != SYMBOL_ZOMBIE) {
				if(map[zombies[i].x][zombies[i].y] != SYMBOL_FIRE)map[zombies[i].x][zombies[i].y] = SYMBOL_EMPTY;
				zombies[i].y--;
			}

			//Zombie die respawn
			if (map[zombies[i].x][zombies[i].y] == SYMBOL_FIRE || map[zombies[i].x][zombies[i].y] == map[player.x][player.y]) {
				// killed zombie by molotov
				if (map[zombies[i].x][zombies[i].y] == SYMBOL_FIRE) {
					if (molotovThrowed) {
						zombieskilled++;
					} else {
						zombieskilled = 0;
					}
					
					switch (zombieskilled) {
						case 2:
							vectorAchiev.push_back(Achievements::A1);
							achievementUnlocked = true;
							break;
						case 3:
							vectorAchiev.push_back(Achievements::A2);
							achievementUnlocked = true;
							break;
						case 4:
							vectorAchiev.push_back(Achievements::A3);
							achievementUnlocked = true;
							break;
					}
					anyZombieKilled = false;
				}

				if (map[zombies[i].x][zombies[i].y] == map[player.x][player.y]) player.life -= 10;
				else player.life += 10;
				int respawn = rand() % 4 + 1;
				switch (respawn) {
					case 1:
						zombies[i].x = 1;
						zombies[i].y = 1;
						break;
					case 2:
						zombies[i].x = 18;
						zombies[i].y = 1;
						break;
					case 3:
						zombies[i].x = 1;
						zombies[i].y = 18;
						break;
					case 4:
						zombies[i].x = 18;
						zombies[i].y = 18;
						break;
				}
			}
			map[zombies[i].x][zombies[i].y] = SYMBOL_ZOMBIE;

			if (achievementUnlocked) {
				cout << "Achievement Unlocked!!" << endl;
				counterFrame++;
				if (counterFrame >= 20) {
					achievementUnlocked = false;
					counterFrame = 0;
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));
		}
	}

}

void showMap() {
	while (player.life > 0) {
		for (int j = 0; j < MapDy; j++) {
			for (int i = 0; i < MapDx; i++) {
				printf("%c", map[i][j]);
			}
			printf("\n");
		}
		std::cout << "Life: " << player.life << "    " << "Turn: " << turn << endl;
		// Achievements time
		if (turn == 100) { // survie 1/2 minute -> turn*GameSpeed|| 100*300 == 1/2 minut
			vectorAchiev.push_back(Achievements::A4);
		} else if (turn >=100 && turn <=110) {
			std::cout << "Achievement unlocked!! survive half minut" << std::endl;
		}

		if (turn == 200) {
			vectorAchiev.push_back(Achievements::A5);
		} else if (turn >= 200 && turn <= 210) {
			cout << "Achievement unlocked!! survive a minut" << endl;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));
		turn++;
		system("cls");
	}
}
void drawTitle() {
	cout << "        _    _   _  ____ ______   __  ________  __  __ ____ ___ _____ ____" << endl;
	cout << "       / |  | | | |/ ___|  _ | | / / |__  / _ ||  |/  | __ )_ _| ____/ ___|" << endl;
	cout << "      / _ | |  || | |  _| |_) | V /    / / | | | ||/| |  _ || ||  _| |___ | " << endl;
	cout << "     / ___ || ||  | |_| |  _ < | |    / /| |_| | |  | | |_) | || |___ ___) |" << endl;
	cout << "    /_/   |_|_| |_||____|_| |_||_|   /____|___/|_|  |_|____/___|_____|____/" << endl;
}

void showMenu() {
	drawTitle();
	cout << "\n\n				   Play (1)\n\n			    	  Ranking (2)\n\n			       Achievements (3)\n\n				   Exit (4)";
	cout << "\nUser : " << player.name << endl;
	if (GetAsyncKeyState(0x31)) {
		std::thread map(showMap);
		std::thread player(playerMove);
		std::thread coctel(ThrowCoctel);
		std::thread zombies(zombieMove);
		player.join();
		zombies.join();
		coctel.join();
		map.join();
		gameState = GameState::game;
	} 
	if (GetAsyncKeyState(0x32)) gameState = GameState::rankings;
	if (GetAsyncKeyState(0x33)) gameState = GameState::achievements;
	if (GetAsyncKeyState(0x34)) gameState = GameState::exit;
	std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));
	system("cls");
}

void menu() {
	showMenu();
}

void game() {
	showMap();
	playerMove();
	ThrowCoctel();
	zombieMove();
}

void rankings() {
	drawTitle();

	sendData("cpun;");


	//read reply
	char acReadBuffer[kBufferSize];
	int nTotalBytes = 0;
	while (nTotalBytes < kBufferSize) {
		int nNewBytes = recv(sd, acReadBuffer + nTotalBytes, kBufferSize - nTotalBytes, 0);

		nTotalBytes += nNewBytes;
	}
	cout << acReadBuffer << endl;
	//////////////////////////////////
	if (GetAsyncKeyState(VK_ESCAPE)) gameState = GameState::menu;
	std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));
	system("cls");

}
void archivements(){
	drawTitle();
	cout << "\n\n\n\n\nBack(esc)";
	if (GetAsyncKeyState(VK_ESCAPE)) gameState = GameState::menu;
	std::this_thread::sleep_for(std::chrono::milliseconds(GameSpeed));
	system("cls");
}
void connectToServer() {
	// Send player points
	string punctuation = "ipun;" + player.name + ";" + to_string(turn);
	sendData((char*)punctuation.c_str());

	// Send player achievemnts
	string achievements = "iach;" + player.name;
	string tmp;
	for (Achievements a : vectorAchiev) {
		achievements += ";";
		switch (a) {
			case Achievements::A1:
				achievements += "1";
				break;
			case Achievements::A2:
				achievements += "2";
				break;
			case Achievements::A3:
				achievements += "3";
				break;
			case Achievements::A4:
				achievements += "4";
				break;
			case Achievements::A5:
				achievements += "5";
				break;
			case Achievements::A6:
				achievements += "6";
				break;
			case Achievements::A7:
				achievements += "7";
				break;
		}
	}
	sendData((char*)achievements.c_str());

}

int main() {
	callServer();
	initializeMap();
	
	while (gameState != GameState::exit) {
		switch (gameState) {
			case GameState::none: 
				break;
			case GameState::menu: 	
				showMenu();
				break;
			case GameState::game:
				if (player.life <= 0) {
					// Any zombie is killed in the game
					if (anyZombieKilled) {
						vectorAchiev.push_back(Achievements::A7);
					}
					gameState = GameState::dead;
				}
				break;
			case GameState::dead: 
				cout << "conect to server" << endl;
				connectToServer();
				gameState = GameState::menu;
				// send data 
				break;
			case GameState::rankings: rankings(); break;
			case GameState::achievements: archivements(); break;
		}
	}
	closeClient();
	return 0;
}